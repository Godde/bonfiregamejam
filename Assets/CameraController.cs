﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    
    [SerializeField]
    private float cameraSpeed = 1;

    private Vector3 targetPosition;
    private Camera cam;
    // Start is called before the first frame update
    void Start()
    {
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        Ray targetRay = cam.ScreenPointToRay(new Vector3(cam.pixelWidth/2, cam.pixelHeight/2));
        RaycastHit outRay;

        if (Physics.Raycast(targetRay, out outRay))
        {
            targetPosition = outRay.point;
        }
        else
        {
            targetPosition = transform.position + targetRay.direction.normalized * 200;
        }

        if (Input.GetAxis("Horizontal") != 0)
        {
            cam.transform.position += Vector3.Cross(cam.transform.forward, Vector3.up).normalized * -Input.GetAxis("Horizontal") *  cameraSpeed*Time.deltaTime;
        }

        if (Input.GetAxis("Vertical") != 0)
        {
            cam.transform.position += new Vector3(cam.transform.forward.x, 0, cam.transform.forward.z) * Input.GetAxis("Vertical") * cameraSpeed * Time.deltaTime;

        }

        if (Input.GetKeyDown(KeyCode.Q))
        {
            //cam.transform.rotation *= Quaternion.AngleAxis(90, Vector3.up);
            Vector3 difference = targetPosition - cam.transform.position;
            cam.transform.position = (Quaternion.Euler(0, 90, 0) * new Vector3(difference.x, 0, difference.z)  ).normalized * difference.magnitude + new Vector3(0,cam.transform.position.y);
            //cam.transform.forward = targetPosition - cam.transform.position;
        }
        if (Input.GetKeyDown(KeyCode.E))
        {

        }

    }

    private void ZoomInAndOutOnMousePosition()
    {
        if (Input.mouseScrollDelta.magnitude != 0)
        {
            Ray targetRay = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit outRay;
            Vector3 target;
            if (Physics.Raycast(targetRay, out outRay))
            {
                target = outRay.point;
            }
            else
            {
                target = transform.position + targetRay.direction.normalized * 200;
            }

            float distance = (transform.position - target).magnitude;
            transform.position += targetRay.direction * Input.mouseScrollDelta.y * distance * 0.1f;
        }
    }
}
