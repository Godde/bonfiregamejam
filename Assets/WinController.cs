﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine.UI;

public class WinController : MonoBehaviour
{
    public TextMeshProUGUI text;
    [SerializeField]
    private int winAmountRequired = 50;
    private int currentAmount = 0;

    [SerializeField]
    private AudioClip winClip;
    [SerializeField]
    private AudioClip loseClip;
    private AudioSource audioSource;
    private bool won = false;
    [SerializeField]
    private Button nextLevelButton;

    //private List<FollowerController> followersInside; //not really needed

    // Start is called before the first frame update
    private void Start()
    {
        audioSource = GetComponent<AudioSource>();
        nextLevelButton.gameObject.SetActive(false);
    }

    public void RestartScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void NextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    // Update is called once per frame
    private void OnTriggerEnter(Collider other)
    {
        if(won == false && other.GetComponent<FollowerController>() != null)
        {
            other.gameObject.SetActive(false);
            text.text = (++currentAmount).ToString();
            if (currentAmount >= winAmountRequired)
            {
                won = true;
                text.text = "You win!";
                if (audioSource.clip != winClip)
                {
                    audioSource.Stop();
                    audioSource.loop = false;
                    audioSource.clip = winClip;
                    audioSource.Play();
                }
                nextLevelButton.gameObject.SetActive(true);
            }
        }
    }

    // Update is called once per frame
    private void OnTriggerExit(Collider other)
    {
        if (won == false && other.GetComponent<FollowerController>() != null)
        {
            text.text = (--currentAmount).ToString();
        }
    }

    

}
