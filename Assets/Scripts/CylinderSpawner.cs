﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CylinderSpawner : MonoBehaviour
{
    public GameObject objectPrefab;
    public int spawnNumber = 100;
    public float spacing = 2;
    public int unitsPerRow = 10;


    void Awake()
    {
        for(int i = 0; i < spawnNumber; i++)
        {
            float x = spacing * (i % unitsPerRow);
            float y = spacing * (i / unitsPerRow);
            Instantiate(objectPrefab, transform.rotation*new Vector3(x, 0, y)+transform.position, transform.rotation);
        }
    }

}
