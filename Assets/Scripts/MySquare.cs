﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySquare : MonoBehaviour
{
    private Plane right1;
    private Plane left2;
    private Plane up3;
    private Plane down4;
    private List<FollowerController> followers;

    [SerializeField]
    public RectTransform selectionSquareTrans;
    // Start is called before the first frame update
    void Start()
    {
        right1 = new Plane();
        left2 = new Plane();
        up3 = new Plane();
        down4 = new Plane();
        followers = new List<FollowerController>( GameObject.FindObjectsOfType<FollowerController>());
    }

    
    public List<FollowerController> CheckFollowers(Ray RU, Ray LU, Ray RLow, Ray LLow)
    {
        right1 = new Plane(Vector3.Cross(RU.direction, RLow.direction), RU.origin);
        left2 = new Plane(Vector3.Cross(LU.direction, LLow.direction), LLow.origin);
        up3 = new Plane(Vector3.Cross(RU.direction, LU.direction), RU.origin);
        down4 = new Plane(Vector3.Cross(RLow.direction, LLow.direction), RU.origin);

        List<FollowerController> returnList = new List<FollowerController>();
        foreach (FollowerController fol in followers)
        {
            Vector3 position = fol.gameObject.transform.position;
            if (right1.GetDistanceToPoint(position) <= 0 
                && left2.GetDistanceToPoint(position) >= 0
                && up3.GetDistanceToPoint(position) >= 0
                && down4.GetDistanceToPoint(position) <= 0


                ) //a bit long lookup
            {
                returnList.Add(fol);
            }
        }


            return returnList;
    }

    public void SetSquarePosition(Vector3 squareStart, Vector3 squareEndPos)
    {
        //Change the size of the square
        float sizeX = Mathf.Abs(squareStart.x - squareEndPos.x);
        float sizeY = Mathf.Abs(squareStart.y - squareEndPos.y);
        //selectionSquareTrans.sizeDelta
        Vector3 middle = (squareStart + squareEndPos) / 2f;

        //Set the middle position of the GUI square
        selectionSquareTrans.position = middle;
        selectionSquareTrans.sizeDelta = new Vector2(sizeX, sizeY);
    }

    public void SetVisible(bool visible) {
        selectionSquareTrans.gameObject.SetActive(visible);
    }

    


}
