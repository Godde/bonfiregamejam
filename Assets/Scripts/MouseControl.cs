﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseControl : MonoBehaviour
{
    private List<FollowerController> selectedUnits; //Should probably be a specific class to the class that can be controlled

    private Vector3 mouseButtonDownPosition;

    [SerializeField]
    private MySquare mySquare;
    private bool mouseButton0pressed = false;

    // Start is called before the first frame update
    void Start()
    {
        selectedUnits = new List<FollowerController>();
    }

    // Update is called once per frame
    void Update()
    {
        if (mouseButton0pressed)
        {
            mySquare.SetSquarePosition(mouseButtonDownPosition, Input.mousePosition);
        }
        ClickDrag();
        if (Input.GetMouseButtonDown(1))
        {
            if (selectedUnits.Count > 0) {
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit outRay;
                if (Physics.Raycast(ray, out outRay))
                {
                    foreach(FollowerController fol in selectedUnits)
                    {
                        fol.SetTarget(outRay.point);
                    }
                }
            }
        }
        if (Input.GetKeyDown(KeyCode.X))
        {
            foreach (FollowerController fol in selectedUnits)
            {
                fol.ExplodeFollower();
            }
        }
        if (Input.GetKeyDown(KeyCode.J))
        {
            foreach (FollowerController fol in selectedUnits)
            {
                fol.JumpToDestination();
            }
        }

    }

    private void ClickDown()
    {
        if (Input.GetMouseButtonDown(0))
        {
            
            foreach (FollowerController fol in selectedUnits)
            {
                fol.SetUnselected();
            }
            selectedUnits.Clear();

            RaycastHit outRay;

            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Debug.Log(" ray");
            if (Physics.Raycast(ray, out outRay)) //should probably have some bitmask here
            {
                //Debug.Log("casted ray");
                FollowerController newSelected = outRay.collider.gameObject.GetComponent<FollowerController>();
                if (newSelected != null)
                {
                    //Debug.Log("hit s");
                    selectedUnits.Add(newSelected);
                    newSelected.SetSelected();
                }
            }
        }
    }

    private void ClickDrag()
    {
        if (Input.GetMouseButtonDown(0))
        {
            mouseButton0pressed = true;
            mySquare.SetSquarePosition(Vector3.zero, Vector3.zero);
            mySquare.SetVisible(true);
            mouseButtonDownPosition = Input.mousePosition;
            foreach (FollowerController fol in selectedUnits)
            {
                fol.SetUnselected();
            }
            selectedUnits.Clear();
        }
        if (Input.GetMouseButtonUp(0))
        {
            mouseButton0pressed = false;
            mySquare.SetVisible(false);
            
            Camera cam = Camera.main;

            Vector3 start = mouseButtonDownPosition;
            Vector3 end = Input.mousePosition;
            //Vector3 someX = new Vector3(mouseButtonDownPosition.x, Input.mousePosition.y, Input.mousePosition.z);
            //Vector3 someY = (new Vector3(Input.mousePosition.x, mouseButtonDownPosition.y, Input.mousePosition.z));
            
            if(Mathf.Abs(start.x-end.x) < 0.5f || Mathf.Abs(start.y - end.y) < 0.5f)
            {
                SingleUnitSelect();
                return;
            }
            Ray RU, LU, RLow, LLow;

            if (start.x > end.x)
            {
                if(start.y > end.y)
                {
                    RU = cam.ScreenPointToRay(start);
                    RLow = cam.ScreenPointToRay(new Vector3(start.x, end.y, start.z));
                    LLow = cam.ScreenPointToRay(end);
                    LU = cam.ScreenPointToRay(new Vector3(end.x, start.y, start.z));
                }
                else
                {
                    RLow = cam.ScreenPointToRay(start);
                    RU = cam.ScreenPointToRay(new Vector3(start.x, end.y, start.z));
                    LU = cam.ScreenPointToRay(end);
                    LLow = cam.ScreenPointToRay(new Vector3(end.x, start.y, start.z));
                }
            }
            else
            {
                if (start.y > end.y)
                {
                    LU = cam.ScreenPointToRay(start);
                    LLow = cam.ScreenPointToRay(new Vector3(start.x, end.y, start.z));
                    RLow = cam.ScreenPointToRay(end);
                    RU = cam.ScreenPointToRay(new Vector3(end.x, start.y, start.z));
                }
                else
                {
                    LLow = cam.ScreenPointToRay(start);
                    LU = cam.ScreenPointToRay(new Vector3(start.x, end.y, start.z));
                    RU = cam.ScreenPointToRay(end);
                    RLow = cam.ScreenPointToRay(new Vector3(end.x, start.y, start.z));
                }
            }



            //Ray start = Camera.main.ScreenPointToRay(mouseButtonDownPosition);
            //Ray end = Camera.main.ScreenPointToRay(Input.mousePosition);
            //Ray someX = Camera.main.ScreenPointToRay(new Vector3(mouseButtonDownPosition.x, Input.mousePosition.y, Input.mousePosition.z));
            //Ray someY = Camera.main.ScreenPointToRay(new Vector3(Input.mousePosition.x, mouseButtonDownPosition.y, Input.mousePosition.z));

            selectedUnits = mySquare.CheckFollowers(RU, LU, RLow, LLow);
            if (selectedUnits.Count > 0)
            {
                foreach (FollowerController fol in selectedUnits)
                {
                    fol.SetSelected();
                }
            }
            else
            {
                SingleUnitSelect();
            }
        }
    }

    private void SingleUnitSelect()
    {
        RaycastHit outRay;

        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //Debug.Log(" ray");
        if (Physics.Raycast(ray, out outRay)) //should probably have some bitmask here
        {
            //Debug.Log("casted ray");
            FollowerController newSelected = outRay.collider.gameObject.GetComponent<FollowerController>();
            if (newSelected != null)
            {
                //Debug.Log("hit s");
                selectedUnits.Add(newSelected);
                newSelected.SetSelected();
            }
        }
    }
}
