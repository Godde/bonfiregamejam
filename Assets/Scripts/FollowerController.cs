﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class FollowerController : MonoBehaviour
{


    private Rigidbody rb;
    private Vector3 target;
    private bool targetActive = false;
    [SerializeField]
    private float moveForceStrength = 1;
    private Material material;
    private int groundCounter = 0;
    [SerializeField]
    private Transform modelTransform;
    private Animator m_Animator;
    [SerializeField]
    private float animatorMult = 1;
    [SerializeField]
    private float jumpHeight = 50;
    //[SerializeField]
    //private float lateralJumpSpeed = 5;
    [SerializeField]
    private float power = 500.0F;

    [SerializeField]
    private float radius = 20.0F;



    // Start is called before the first frame update
    void Start()
    {
        material = GetComponent<MeshRenderer>().material;
        material.color = Color.blue;
        rb = GetComponent<Rigidbody>();
        rb.sleepThreshold = 0;
        m_Animator = gameObject.GetComponentInChildren<Animator>();
    }

    private void Update()
    {
    if (modelTransform != null)
    {
        Vector3 forward = new Vector3(rb.velocity.x, 0, rb.velocity.z);
            m_Animator.speed = forward.magnitude * animatorMult;
        if (forward.magnitude > 0.1)
        {
            modelTransform.forward = forward;
        }
    }
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (targetActive && groundCounter > 0)
        {

            rb.AddForce((target - transform.position).normalized * moveForceStrength);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.rigidbody == null)
        {   
            groundCounter++;
        }
    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.rigidbody == null)
            groundCounter--;
    }

    public void SetTarget(Vector3 target)
    {
        this.target = target;
        targetActive = true;
        material.color = Color.red;
    }

    public void SetTargetInactive()
    {
        targetActive = false;
        material.color = Color.green;
    }

    public void SetSelected()
    {
        material.color = Color.white;
    }

    public void SetUnselected()
    {
        material.color = Color.yellow;
    }

    public void ExplodeFollower()
    {

        Vector3 explosionPos = transform.position;
        LayerMask mask = LayerMask.GetMask("Rigidbody");
        Collider[] colliders = Physics.OverlapSphere(explosionPos, radius, mask);
        foreach (Collider hit in colliders)
        {
            //Rigidbody rb = hit.GetComponent<Rigidbody>();
            

            //if (rb != null)
                hit.attachedRigidbody.AddExplosionForce(power, explosionPos, radius, 3.0F);
        }
        gameObject.SetActive(false);
    }

    public Vector3 ParabolaTrajectory(Vector3 targetDestination, Vector3 targetDeparture, float jumpHeight)
    {
        Vector3 targetVector = targetDestination - targetDeparture;
        Vector2 lateralVector = new Vector2(targetVector.x, targetVector.z);
        float flatDistance = lateralVector.magnitude;
        
        float jumpSpeed = Mathf.Pow(jumpHeight * -Physics.gravity.y, 0.5f);
        float travelTime = jumpSpeed/ -Physics.gravity.y + Mathf.Pow((jumpHeight-targetVector.y) * -Physics.gravity.y, 0.5f)/ -Physics.gravity.y;
        float horizontalSpeed = flatDistance / travelTime;
        //float upWardsSpeed = Mathf.Pow(((flatDistance*0.5f) / lateralSpeed), 0.5f);
        //float verticalDistance = targetVector.y;
        //float upWardsSpeed = ((flatDistance * 0.5f) / lateralJumpSpeed) * Physics.gravity.magnitude;

        Debug.Log(Mathf.Pow(50, 0.5f));
        Debug.Log("jumpSpeed " + jumpSpeed);
        return new Vector3(0, jumpSpeed, 0) + new Vector3(lateralVector.x, 0, lateralVector.y).normalized * horizontalSpeed;
        //return new Vector3(0, 0, 0)+new Vector3(lateralVector.x, 0, lateralVector.y).normalized*lateralSpeed;
        //return targetVector.normalized*lateralSpeed;
    }

    //public Vector3 ParabolaTrajectory(Vector3 targetDestination, Vector3 targetDeparture)
    //{
    //    Vector3 targetVector = targetDestination - targetDeparture;
    //    Vector2 lateralVector = new Vector2(targetVector.x, targetVector.z);
    //    float flatDistance = lateralVector.magnitude;


    //    //float upWardsSpeed = Mathf.Pow(((flatDistance*0.5f) / lateralSpeed), 0.5f);
    //    float verticalDistance = targetVector.y;
    //    float upWardsSpeed = ((flatDistance*0.5f) / lateralJumpSpeed) *Physics.gravity.magnitude;

    //    return new Vector3(0, upWardsSpeed, 0) + new Vector3(lateralVector.x, 0, lateralVector.y).normalized * lateralJumpSpeed;
    //    //return new Vector3(0, 0, 0)+new Vector3(lateralVector.x, 0, lateralVector.y).normalized*lateralSpeed;
    //    //return targetVector.normalized*lateralSpeed;
    //}

    public void JumpToDestination()
    {
        if(groundCounter > 0)
        rb.AddForce(ParabolaTrajectory(target, transform.position, jumpHeight * 2) -rb.velocity, ForceMode.VelocityChange);
    }
}
